import unittest

from github_api.v3.github_api_request import GithubApiRequest


class TestGithubApiRequest(unittest.TestCase):

    def test_initialize_github_api_request(self):
        gar = GithubApiRequest(username='someone', password='secret')
        self.assertIsInstance(gar, GithubApiRequest)

    def test_initialize_fail_blank_user(self):
        self.assertRaises(ValueError, lambda: GithubApiRequest(username='', password='secret'))

    def test_initialize_fail_blank_password(self):
        self.assertRaises(ValueError, lambda: GithubApiRequest(username='someone', password=''))