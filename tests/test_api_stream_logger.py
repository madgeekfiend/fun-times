import unittest

from github_api.logger.api_stream_logger import ApiStreamLogger


class TestApiStreamLogger(unittest.TestCase):

    def test_initialize(self):
        """
        Object should exist with nothing
        """
        logger = ApiStreamLogger()
        self.assertIsInstance(logger, ApiStreamLogger)

    def test_initialize_with_logger_name(self):
        logger = ApiStreamLogger(logger_name='TestLogger')
        self.assertIsInstance(logger, ApiStreamLogger)
        self.assertEqual('TestLogger', str(logger))

    def test_logger_set_level(self):
        logger = ApiStreamLogger()
        logger.set_logging_level(ApiStreamLogger.INFO_LEVEL)
        self.assertEqual(ApiStreamLogger.INFO_LEVEL, logger.get_logging_level())