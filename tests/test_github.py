import unittest
from github_api.v3.github import Github


class GithubTestCase(unittest.TestCase):

    def test_initialize_raises_error_username_password_blank(self):
        self.assertRaises(ValueError, lambda: Github('', ''))

    def test_initialize_raises_error_username_blank(self):
        self.assertRaises(ValueError, lambda: Github('', 'password'))

    def test_initialize_raises_error_password_blank(self):
        self.assertRaises(ValueError, lambda: Github('user', ''))

    def test_initialize_github(self):
        github = Github('user', 'password')
        self.assertIsInstance(github, Github)
