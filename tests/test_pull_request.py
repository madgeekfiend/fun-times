import unittest

from github_api.v3.github_api_request import GithubApiRequest
from github_api.v3.pull_request import PullRequest


class TestPullRequest(unittest.TestCase):

    def test_initialize_pull_request(self):
        pr = PullRequest(GithubApiRequest(username='someone', password='secret'))
        self.assertIsInstance(pr, PullRequest)
