FROM python:3.6.3-slim
MAINTAINER scontapay@gmail.com

RUN mkdir -p /usr/app
WORKDIR /usr/app

RUN apt-get -y update && \
    apt-get -y install build-essential

COPY . /usr/app

RUN pip install -r requirements.txt
