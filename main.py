from github_api.logger.api_stream_logger import ApiStreamLogger
from github_api.v3.github import Github

# Run this to test the API created
if __name__ == "__main__":
    repo_owner = "ramda"
    repo_name = "ramda"

    username = "<insert your username>"
    password = "<insert your password>"

    logger = ApiStreamLogger(logger_name='Main')
    gh = Github(username, password)
    # Get list of pull requests
    all_pull_requests = gh.pull_requests.all(repo_owner, repo_name)
    open_pull_requests = gh.pull_requests.open(repo_owner, repo_name)
    closed_pull_requests = gh.pull_requests.close(repo_owner, repo_name)
    logger.log_info("Retrieved all Pull Request object count: {0}".format(len(all_pull_requests)))
    logger.log_info("Retrived open Pull Request object count: {}".format(len(open_pull_requests)))
    logger.log_info("Retrived close Pull Request object count: {}".format(len(closed_pull_requests)))
