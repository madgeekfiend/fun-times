"""
File to hold all general settings for Github v3 API

Eventually this can move to environmental vars
"""

GITHUB_API_ROOT_URL="https://api.github.com"
# Explicitly request all calls with header
GITHUB_REQUEST_V3_HEADER="application/vnd.github.v3+json"