import requests
from requests.auth import HTTPBasicAuth

from github_api.logger.api_stream_logger import ApiStreamLogger


class GithubApiRequest:
    """
    Github API request
    """

    # User-Agent
    _API_USER_AGENT = "GITHUB-API-REQUEST-API"
    # Accept
    # Explicitly request the API v3 API
    _REQUEST_API_VERSION = "application/vnd.github.v3+json"

    def __init__(self, username, password, items_per_page=100):
        """
        Initialize GithubApRequest

        :param username: github username
        :param password: github password
        :param items_per_page: Defaults to 100 and may not work on some REST endpoints
        """

        if not username or not password:
            raise ValueError('You must supply both a username and password.')
        self.username = username
        self.password = password
        # I don't think this works for the PR list URL
        self._items_per_page = items_per_page
        self.logger = ApiStreamLogger(logger_name="GithubApiRequest")

    def request_get(self, url, headers={}, query_params={}):
        """
        Standard request to a GITHUB API GET endpoint

        :param query_params: parameters passed to the API call
        :param url: Fully qualified URL of the REST endpoint you want to hit
        :param headers: Dictionary of headers
        :return: JSON or error
        """
        headers.update({
            'Accept': GithubApiRequest._REQUEST_API_VERSION,
            'User-Agent': GithubApiRequest._API_USER_AGENT
        })

        # Check params to only send something with value
        if 'head' in query_params and query_params['head'] is None:
            query_params.pop('head', None)
        if 'base' in query_params and query_params['base'] is None:
            query_params.pop('base', None)

        self.logger.log_debug("Sending these headerss: {}".format(headers))

        data = []

        # Tossed up between making this recursive. Is there any benefit? Python needs to have a Do...while loop
        while True:
            response = requests.get(url, headers=headers, params=query_params,
                                    auth=HTTPBasicAuth(self.username, self.password))

            if response.status_code == 200 or response.status_code == 304:
                # Logger for DEBUGGING
                self.logger.log_debug(
                    "Rate Limit Remaining: {0} Response Code: {1}".format(response.headers['X-RateLimit-Remaining'],
                                                                          response.status_code))
                self.logger.log_debug("Link Header {0}".format(response.headers['Link']))
                if response:
                    data += response.json()
                    self.logger.log_info("Current 'data' object count: {0}".format(len(data)))
                if 'next' not in response.links:
                    break
                else:
                    self.logger.log_debug("Setting url to {0} because NEXT header found.".format(response.links['next']))
                    url = response.links['next']['url']

        self.logger.log_debug("Number Objects in data: {}".format(len(data)))
        return data
