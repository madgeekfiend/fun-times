from .github_api_request import GithubApiRequest
from .pull_request import PullRequest


class Github:
    """
    Github class

    Class that handles all connection calls to the github API. This is a wrapper to
    all the v3 REST APIs in Python.
    """

    def __init__(self, username, password):
        """
        Construct a new Github object

        This object will hold the API username and password

        :param username: Username of Github user
        :param password: Plain text password of Github user
        """
        if not username or not password:
            raise ValueError("You must intialize with a value")
        self.username = username
        self.password = password
        # Setup request object
        self.request = GithubApiRequest(self.username, self.password)

        # Setup different layers of the API. For now isolate and contain in
        # different classes. Then use the main Github object as a wrapper. Facade
        # pattern.
        #
        # Use Dependency Injection to inject request object into each class.
        self.pull_requests = PullRequest(self.request)
