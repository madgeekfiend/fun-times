from github_api.logger.api_stream_logger import ApiStreamLogger
from .settings import *
from .github_api_request import GithubApiRequest


class PullRequest():
    # What results to sort by
    SORT_CREATED = 'created'
    SORT_UPDATED = 'updated'
    SORT_POPULARITY = 'popularity'
    SORT_LONG_RUNNING = 'long-running'

    # Which direction to sort
    SORT_DIRECTION_ASCENDING = 'asc'
    SORT_DIRECTION_DESCENDING = 'desc'

    def __init__(self, request_object):
        """
        Initialize Pull Request object

        :param request_object: Type of request object, you can use it for MOCK API endpoints
        """
        if request_object is None or not isinstance(request_object, GithubApiRequest):
            raise ValueError('You must initialize with a GithubApiRequest object.')

        self.github_api_request = request_object  # type: GithubApiRequest
        self.logger = ApiStreamLogger(logger_name="PullRequest", logging_level=ApiStreamLogger.INFO_LEVEL)

    def _get_pull_requests(self, owner, repo, params={}):
        """
        Base get request for Pull Request

        :param owner: Repo owner
        :param repo: Repo name
        :param params: Query parameters to pass to Githup API v3 REST endpoint
        :return:
        """
        # check to make sure owner and repo are not null
        if owner is None or repo is None:
            raise ValueError('Owner or repo must have a value.')

        request_url_path = GITHUB_API_ROOT_URL + "/repos/{0}/{1}/pulls".format(owner, repo)
        return self.github_api_request.request_get(request_url_path, headers={}, query_params=params)

    def all(self, owner, repo, sort_by=SORT_CREATED, sort_direction=SORT_DIRECTION_ASCENDING, head=None, base=None):
        """
        Get all pull requests for given head and base.

        :param owner: Repo owner
        :param repo: Repo name
        :param sort_by: Sort by criteria static variables on PullRequest Class
        :param sort_direction: Direction to sort. Default 'asc'.
        :param head: Name
        :param base: Base repo
        :return:  array of pull requests
        """
        self.logger.log_info("Call to all() with params: {}".format(locals()))
        params = {'state': 'all', 'sort': sort_by, 'direction': sort_direction, 'head': head, 'base': base}
        return self._get_pull_requests(owner, repo, params)

    def open(self, owner, repo, sort_by=SORT_CREATED, sort_direction=SORT_DIRECTION_ASCENDING, head=None, base=None):
        """
        Get all the open pull requests

        :param owner: Repo owner
        :param repo: Repo name
        :param sort_by: Criteria to sort by
        :param sort_direction: Direction to sort by. Defaults to ascending.
        :param head: github head
        :param base: github base
        :return: array of pull requests
        """
        self.logger.log_info("Call to open() with params: {}".format(locals()))
        params = {'state': 'open', 'sort': sort_by, 'direction': sort_direction, 'head': head, 'base': base}
        return self._get_pull_requests(owner, repo, params)

    def close(self, owner, repo, sort_by=SORT_CREATED, sort_direction=SORT_DIRECTION_ASCENDING, head=None, base=None):
        """
        Get all closed pull requests

        :param owner: owner name
        :param repo:  repo name
        :param sort_by: Criteria to sort by
        :param sort_direction: Order to sort by. Defaults to ascending.
        :param head: repo head
        :param base: repo base
        :return: array of pull requests
        """
        self.logger.log_info("Call to close() with params: {}".format(locals()))
        params = {'state': 'closed', 'sort': sort_by, 'direction': sort_direction, 'head': head, 'base': base}
        return self._get_pull_requests(owner, repo, params)
