import logging


class ApiStreamLogger:

    DEBUG_LEVEL = logging.DEBUG
    INFO_LEVEL = logging.INFO

    def __init__(self, logger_name=None, logging_level=DEBUG_LEVEL):
        """
        Wrapper around Python logging to isolate logger instantiation

        :param logger_name: Name of logger should be related to what you are logging
        :param logging_level: Debug by default. This should be changed to use an environmental variable
        """
        self.logger = logging.getLogger(logger_name)
        self.handler = logging.StreamHandler()
        self.formatter = logging.Formatter(
            '%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
        self.handler.setFormatter(self.formatter)
        self.logger.addHandler(self.handler)
        self.logger.setLevel(logging_level)

    def __str__(self):
        return self.logger.name

    def set_logging_level(self, logging_level=DEBUG_LEVEL):
        """
        Set logging level

        :param logging_level: Set to either DEBUG_LEVEL or INFO_LEVEL

        :return: nothing
        """
        self.logger.setLevel(logging_level)

    def get_logging_level(self):
        return self.logger.level

    def log_debug(self, message):
        """
        Log a debug message
        :param message: String of message
        :return:
        """
        self.logger.debug(message)

    def log_info(self, message):
        """
        Log an info message

        :param message: String of message you want to log
        :return:
        """
        self.logger.info(message)
