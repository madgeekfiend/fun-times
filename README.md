Github API allows you to use the Pull Request API to retrieve a list of Pull Requests.

# Setup

You can use 2 ways to setup github API. This was written using Python 3.6.3. Before you run the code make sure you update `main.py` with your Github username and password where it says.

# Libraries Used

*Requests* http://docs.python-requests.org/en/master/

No boilerplate code generated

# Object Oriented Design Decision

I chose to use the Github v3 REST API because I'm not an export in GraphQL. WIth my current time restraints due to work I did not have enough time to dig into it. I decided to use a facade pattern to wrap the Github API.

There is a main API request object that takes your username and password. I seperate API calls into different classes for maintainability. The package namespacing is what I would use to control API versioning. If Github creates a v4 API we would make a v4 package this will prevent production code from breaking.

## Docker

Inside is a Dockerfile. First build the image

* cd /-into code directory- (This has the Dockerfile)
* Open `main.py` and replace username and password with your GITHUB username and password
* Run `docker build -t github-api .`. This will create an image with the name `github-api`.
* Execute the script `docker run -t github-api python main.py`
* At completion you will see a count of **ALL** open pull requests on ramda/ramda

## Python Virtual Env

* Create virtual environment. I would use `pyenv virtualenv github-api`
* Activate virtual environment by running `pyenv activate github-api`
* cd into directory with code
* Run `pip install -r requirements.txt`
* Execute code by running `python main.py`

The above instructions assumed you used Pyenv to install version 3.6.3

## Unit Tests

Test coverage is not where I would like it to be. To run the existing tests:

* cd *into code directory*
* Type `python -m unittest`
* To use docker image you built run `docker run -t github-api python -m unittest`

# Design Decisions

I focused on creating a wrapper around the Github v3 API. I tried to follow the facade patter with this project. The API I
felt was more of a data repository. I formed an API around  this. The testing can have more coverage.

# Future Design Goals

* Use API caching
* If a microservice consider using Redis or MemcacheDB
* Use conditional requests to allow for better use of API usage
* Better error catching and handling of error API


